package com.example.newsprogect.database.crud;

import android.content.ContentValues;
import android.content.Context;


import com.example.newsprogect.Constants;
import com.example.newsprogect.database.NewsDatabaseHelper;
import com.example.newsprogect.database.bdpojo.NewsEntity;
import com.example.newsprogect.network.responce.Quote;

import java.util.List;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * Created by kostya on 8/12/17.
 */

public class Crud {

    private NewsDatabaseHelper databaseHelper;

    public Crud(Context context) {
        databaseHelper = new NewsDatabaseHelper(context);

    }

    public void createOrUpdateObject(List<Quote> newsEntityList) {
        List<NewsEntity> listRepo = readAllData(null);
        for (NewsEntity newsEntity : newsEntityList) {
            boolean isState = false;
            for (NewsEntity omletEntityRepo : listRepo) {
                if (newsEntity.getCaption().trim().equals(omletEntityRepo.getCaption().trim())) {
                    isState = true;
                }
            }
            if (!isState) {
                createObject(newsEntity);

            } else {
                update(newsEntity);
            }
        }
    }
    private void update(NewsEntity newsEntity){
        ContentValues values = new ContentValues();
        values.put(Constants.TYPE, newsEntity.getType());
        values.put(Constants.CONTENT, newsEntity.getContent());
        cupboard().withDatabase(databaseHelper.getWritableDatabase()).update(NewsEntity.class, values, Constants.CAPTION+" = ?", newsEntity.getCaption());


    }

    public void delete(String title){
        cupboard().withDatabase(databaseHelper.getWritableDatabase()).delete(NewsEntity.class, Constants.CAPTION+" = ?", "" + title);

    }

    private  void createObject(NewsEntity newsEntity){
        long id = cupboard().withDatabase(databaseHelper.getWritableDatabase()).put(newsEntity);
    }

    public List<NewsEntity> readAllData(String search) {
        if (search != null && search.length() > 0) {
            return cupboard().withDatabase(databaseHelper.getReadableDatabase()).query(NewsEntity.class).withSelection(Constants.CAPTION+" like ? limit 10", "%" + search + "%").list();
        } else {
            return cupboard().withDatabase(databaseHelper.getReadableDatabase()).query(NewsEntity.class).list();
        }

    }

}
