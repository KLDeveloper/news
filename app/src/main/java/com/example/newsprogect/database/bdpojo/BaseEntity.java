package com.example.newsprogect.database.bdpojo;

import java.io.Serializable;

/**
 * Created by kostya on 8/12/17.
 */

public class BaseEntity implements Serializable {
    public Long _id;

    public Long get_id() {
        return _id;
    }

    public void set_id(Long _id) {
        this._id = _id;

    }
}