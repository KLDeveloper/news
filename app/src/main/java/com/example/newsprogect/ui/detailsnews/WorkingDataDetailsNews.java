package com.example.newsprogect.ui.detailsnews;

import android.content.Context;

import com.example.newsprogect.database.crud.Crud;
import com.example.newsprogect.network.NewsApiFactory;
import com.example.newsprogect.network.responce.News;
import com.example.newsprogect.network.responce.Quote;
import com.example.newsprogect.ui.core.BaseActivity;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by kostya on 2/1/18.
 */

public class WorkingDataDetailsNews {
    private Context context;
    private Crud crud;
    private IDetailNews iDetailNews;

    public WorkingDataDetailsNews(Context context, IDetailNews iDetailNews) {
        this.context = context;
        crud = new Crud(context);
        this.iDetailNews = iDetailNews;
    }

    public void getCheckingDetails(String s) {
        ((BaseActivity)context).showLoadingProgress(getClass().getName());
        if(((BaseActivity)context).isChekingNetwork()) {
            NewsApiFactory.getNewsApi(context).getCheckingNews().enqueue(new Callback<News>() {
                @Override
                public void onResponse(Call<News> call, Response<News> response) {
                    if (response.body() != null) {
                        ((BaseActivity) context).dismissLoadingProgress();
                        boolean isChange = false;
                        for (Quote quote : response.body().getContents().getQuotes()) {
                            if (quote.getCaption().toLowerCase().equals(s.toLowerCase())) {
                                isChange = true;
                                break;
                            }
                            ;
                        }
                        if (isChange) {
                            crud.createOrUpdateObject(response.body().getContents().getQuotes());
                            iDetailNews.responce(crud.readAllData(s).get(0));
                        } else {
                            iDetailNews.responce(crud.readAllData(s).get(0));
                        }
                    }
                }

                @Override
                public void onFailure(Call<News> call, Throwable t) {
                    ((BaseActivity) context).dismissLoadingProgress();
                    iDetailNews.responce(crud.readAllData(s).get(0));
                }
            });
        }else {
            ((BaseActivity) context).dismissLoadingProgress();
            iDetailNews.responce(crud.readAllData(s).get(0));
        }
    }




}
