package com.example.newsprogect.ui.detailsnews;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.widget.TextView;

import com.example.newsprogect.Constants;
import com.example.newsprogect.R;
import com.example.newsprogect.database.bdpojo.NewsEntity;
import com.example.newsprogect.ui.core.BaseActivity;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by kostya on 1/27/18.
 */

public class DetailNews extends BaseActivity {

    @Bind(R.id.title)
    TextView title;
    @Bind(R.id.desc)
    TextView desc;
    ActionBar actionBar;
    WorkingDataDetailsNews work;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);

        initActionBar();
        work = new WorkingDataDetailsNews(this, iDetailNews);
        getInfo();

    }

    private void initActionBar() {
        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);

        }

    }

    IDetailNews iDetailNews = new IDetailNews() {
        @Override
        public void responce(NewsEntity newsEntity) {
            setInfo(newsEntity);
            setResult(Activity.RESULT_OK);

        }
    };

    private void setInfo(NewsEntity newsEntity) {
        title.setText(newsEntity.getCaption());
        actionBar.setTitle(newsEntity.getType().toUpperCase());
        desc.setText(newsEntity.getContent());

    }

    private void getInfo() {
        Intent intent = getIntent();
        String titleText = intent.getStringExtra(Constants.TITLE);
        work.getCheckingDetails(titleText);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

}
