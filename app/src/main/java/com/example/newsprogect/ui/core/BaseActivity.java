package com.example.newsprogect.ui.core;

import android.content.Context;
import android.net.ConnectivityManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.example.newsprogect.ui.dialog.ProgressDialog;

/**
 * Created by kostya on 2/1/18.
 */

public abstract class BaseActivity extends AppCompatActivity{

    protected ProgressDialog progressDialog = new ProgressDialog();

    public void showLoadingProgress(String tag) {
        android.app.Fragment progress = getFragmentManager().findFragmentByTag(tag);
        if(progress != null) {
            getFragmentManager().beginTransaction().show(progress).commit();
        } else {
            if(progressDialog != null) {
                progressDialog.show(getFragmentManager(), "progress");
            }
        }
    }


    public void dismissLoadingProgress() {
        if(progressDialog != null) {
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(progressDialog != null) {
                        progressDialog.dismiss();
                    }
                }
            });
        }
    }

    public boolean isChekingNetwork() {
        final ConnectivityManager connMgr = (ConnectivityManager) this
                .getSystemService(Context.CONNECTIVITY_SERVICE);


        final android.net.NetworkInfo wifi = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        final android.net.NetworkInfo mobile = connMgr
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (wifi.isConnected() || mobile.isConnected()) {
            return true;

        } else {
            return false;
        }

    }
}
