package com.example.newsprogect.ui.mainactivity;


import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.example.newsprogect.Constants;
import com.example.newsprogect.ui.adapter.NewsAdapter;
import com.example.newsprogect.R;
import com.example.newsprogect.database.bdpojo.NewsEntity;
import com.example.newsprogect.ui.core.BaseActivity;
import com.example.newsprogect.ui.detailsnews.DetailNews;



import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;


public class MainActivity extends BaseActivity {

    @Bind(R.id.rv)
    RecyclerView recyclerView;
    private NewsAdapter newsAdapter;
    private List<NewsEntity> list;

    private WorkingDataMainActivity workingData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initActionBar();
        initRv();
        workingData = new WorkingDataMainActivity(this, iMainActivity);
        workingData.getListDefult();
    }


    IMainActivity iMainActivity = new IMainActivity() {
        @Override
        public void response(List<NewsEntity> newsEntities) {
            list.clear();
            list.addAll(newsEntities);
            newsAdapter.notifyDataSetChanged();
        }
    };

    private void initActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(getString(R.string.news_title));
            actionBar.setDisplayShowTitleEnabled(true);
            actionBar.setHomeButtonEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(false);
        }

    }


    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            NewsEntity news = (NewsEntity) view.getTag();
            startActivityForResult(new Intent(MainActivity.this, DetailNews.class)
                    .putExtra(Constants.TITLE, news.getCaption()), 1);
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                workingData.selected();
            }
        }
    }

    private void initRv() {
        list = new ArrayList<NewsEntity>();
        newsAdapter = new NewsAdapter(this, list, listener);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(newsAdapter);
    }

}
