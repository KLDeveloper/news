package com.example.newsprogect.ui.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.newsprogect.R;
import com.example.newsprogect.database.bdpojo.NewsEntity;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by kostya on 8/12/17.
 */

public class NewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    List<NewsEntity> list;
    Context context;
    View.OnClickListener listener;

    public NewsAdapter(Context context, List<NewsEntity> list, View.OnClickListener listener){
        this.context = context;
        this.list = list;
        this.listener = listener;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new NewsAdapter.Holder(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_news, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
      Holder hold = (Holder)holder;
        hold.title.setText(list.get(position).getCaption().trim());

            hold.desc.setText(list.get(position).getType().trim());

      //  hold.desc.setText(list.get(position).getType().trim());
        hold.itemView.setTag(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class Holder extends RecyclerView.ViewHolder {
       @Bind(R.id.title)
       TextView title;
        @Bind(R.id.type)
        TextView desc;

        public Holder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(listener);
        }

    }
}
