package com.example.newsprogect.ui.mainactivity;

import android.content.Context;

import com.example.newsprogect.database.bdpojo.NewsEntity;
import com.example.newsprogect.database.crud.Crud;
import com.example.newsprogect.network.NewsApiFactory;
import com.example.newsprogect.network.responce.News;
import com.example.newsprogect.network.responce.Quote;
import com.example.newsprogect.ui.core.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by kostya on 2/1/18.
 */

public class WorkingDataMainActivity {
    private Context context;
    private Crud crud;
    private IMainActivity iMainActivity;

    public WorkingDataMainActivity(Context context, IMainActivity iMainActivity) {
        this.context = context;
        crud = new Crud(context);
        this.iMainActivity = iMainActivity;
    }

    public void getListDefult() {
        ((MainActivity)context).showLoadingProgress(getClass().getName());
        if(((BaseActivity)context).isChekingNetwork()) {
            NewsApiFactory.getNewsApi(context).getNewsData().enqueue(new Callback<News>() {
                @Override
                public void onResponse(Call<News> call, Response<News> response) {
                    if (response.body() != null) {
                        asyncDelete(response.body().getContents().getQuotes());
                    }

                }

                @Override
                public void onFailure(Call<News> call, Throwable t) {
                    selected();

                }
            });
        }else{
            selected();
        }
    }


    private void asyncDelete(List<Quote> listService) {
        Observable.fromCallable(() -> deleteOldNews(listService))
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(isResult -> {
                    if(isResult) {
                      crud.createOrUpdateObject(listService);
                      selected();
                    }
                });
    }

    public void selected(){
        iMainActivity.response(crud.readAllData(null));
        ((MainActivity)context).dismissLoadingProgress();
    }

    private boolean deleteOldNews(List<Quote> listService) {
        List<NewsEntity> listBD = new ArrayList<>();
        listBD.addAll(crud.readAllData(null));
        for (NewsEntity newsEntity : listBD) {
            boolean isDeleteNews = false;
            for (NewsEntity newsServer : listService) {
                if (newsEntity.getCaption().toLowerCase().equals(newsServer.getCaption().toLowerCase())) {
                    break;
                } else {
                    isDeleteNews = true;
                }
            }

            if (isDeleteNews) {
                crud.delete(newsEntity.getCaption());
            }
        }
        return true;
    }

}
