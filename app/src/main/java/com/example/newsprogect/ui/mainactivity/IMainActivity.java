package com.example.newsprogect.ui.mainactivity;

import com.example.newsprogect.database.bdpojo.NewsEntity;

import java.util.List;

/**
 * Created by kostya on 2/1/18.
 */

public interface IMainActivity {
    void response(List<NewsEntity> newsEntities);
}
