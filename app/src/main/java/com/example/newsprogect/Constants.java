package com.example.newsprogect;

/**
 * Created by kostya on 2/1/18.
 */

public class Constants {
    public static final String TITLE = "title";
    public static final String TYPE = "type";
    public static final String DESC = "desc";
    public static final String CAPTION = "caption";
    public static final String CONTENT = "content";


}
