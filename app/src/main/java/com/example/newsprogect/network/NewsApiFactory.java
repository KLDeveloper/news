package com.example.newsprogect.network;


import android.content.Context;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;



public class NewsApiFactory {
    private static NewsApi serviceApi;
    private static Retrofit retrofit;


    public static NewsApi getNewsApi(Context context) {


        final OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new FakeInterceptor(context))
                .build();


        Retrofit.Builder builder = new Retrofit.Builder();
        builder.baseUrl("http://mock.api");
        builder.client(okHttpClient);
        builder.addConverterFactory(GsonConverterFactory.create());


        serviceApi = builder.build().create(NewsApi.class);
        return serviceApi;
    }
}
