package com.example.newsprogect.network;


import com.example.newsprogect.network.responce.News;

import retrofit2.Call;
import retrofit2.http.GET;


public interface NewsApi {
    @GET("/jnews")
    Call<News> getNewsData();
    @GET("/jcheking")
    Call<News> getCheckingNews();
}